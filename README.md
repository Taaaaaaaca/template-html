# ファイルのエンコードは、UTF-8（BOMなし）を使用する
HTMLファイルのエンコーディングは、バイトオーダーマークなしのUTF-8を使用する。  
HTMLファイルは、ドキュメント内でエンコードを、`<meta charset="utf-8" />`などと指定する。  
HTMLファイルのドキュメント内では、スタイルシートのエンコードを仮定・指定しない。  
スタイルシートファイルは、そのドキュメント内でエンコードを適宜指定する。  
CSSファイルでUTF-8（BOMなし）であれば、`@charset="utf-8";`などと指定。
<br>
<br>
<br>

# パスの指定方法
相対パス、ルートパスのどちらかで統一する。  
※ 相対パスの場合`<img src="img/logo.png">`ではなく`<img src="./img/logo.png">`で統一する。
<br>
<br>
<br>

# インデントは半角スペース2個
インデントは半角スペース4個とし、タブは使用しない。  
コードの難読化回避・可読性向上のために用いる半角スペースは例外とする。
## HTMLの推奨例
```
<ul>
  <li>One</li>
  <li>Two</li>
</ul>
```
## CSSの推奨例
```
.example {
  color: blue;
}
```
<br>
<br>

# 半角英数字の小文字を使用する
HTMLの各要素、属性、テキストとCDATAを除く属性値、CSSのセレクタ、プロパティ名、文字列を除くプロパティ値などの記述には半角英数字の小文字を使用する。
## HTML
### 非推奨例
```
<IMG CLASS="EXAMPLE" SRC="google.png" ALT="Google" />
```
### 推奨例
```
<img class="example" src="google.png" alt="Google" />
```
<br>

## CSS
### 非推奨例
```
.EXAMPLE {
  color: #E5E5E5;
}
```
### 推奨例
```
.example {
  color: blue;
}
```
<br>
<br>

# 不要な空白文字は削除する
インデント・コードの難読化回避・コードの可読性向上のために用いる以外で、必要性のない空白文字は削除する。
## HTMLの非推奨例
```
<div>
__<p>This_<span>is</span>_a_pen.</p>＿
</div>_
```
## HTMLの推奨例
```
<div>
__<p>This_<span>is</span>_a_pen.</p>
</div>
```
※ 上記の例は、アンダーバー（_）を空白文字（半角スペース）とした場合。  
<br>
<br>
<br>

# コードの機能を説明するコメントを残す
保守管理しやすいように、必要に応じてコードの機能をコメントする。  
CSSは、可読性を向上させるため、セクションごとに見出しを作る。
## HTMLの例
```
<!-- Google Analytics Tracking Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-00000000-0', 'auto');
  ga('send', 'pageview');
</script>
```
## CSSの例
```
/* float の回り込みを解除 */
.clearfix:after {
  content: ".";
  display: block;
  visibility: hidden;
  clear: both;
  width: 0px;
  height: 0px;
  font-size: 0px;
  line-height: 0px;
  overflow: hidden;
}
```
## CSSの見出し例
```
/* =====================================
*  CSSの見出し1
* ===================================== */


/*---------------------------------------------
  CSSの見出し2
---------------------------------------------*/


/* CSSの見出し3
---------------------------------------------*/


/*-- CSSの見出し4 --*/
```
<br>
<br>

# コメントアウトでのコード無効化は行わない
HTML/CSSでの<!-- -->や/* */を使用したコードの無効化は、Web上で可視化できるうえ、ファイルサイズの膨張とコードの難読化に繋がるため、行わない。  
コードを無効化する場合は、完全に削除する。  
HTMLで、コードの可読性向上のための改行のコメントアウトは例外とする。
## CSSの非推奨例
```
.example {
  margin: 20px auto;
  /*
  max-widht: 80%;
  max-height: 500px;
  */
  font-size: 14px;
}
```
## CSSの推奨例
```
.example {
  margin: 20px auto;
  font-size: 14px;
}
```
## 改行の無効化
```
<label><!--
  -->I am confident that novice users of markup can instantly see any code.<!--
  --><input type="radio" name="radio1" value="0" /><!--
--></label>
```
<br>
<br>

# HTMLの引用符
HTMLの引用符は、二重引用符を使用する。
## 非推奨例
```
<a class='button-secondary'>Sign in</a>
```
## 推奨例
```
<a class="button-secondary">Sign in</a>
```
<br>
<br>

# CSSの引用符
CSSの引用符は、単一引用符を使用する。  
CSSのURL指定には、引用符を使用しない。  
CSSの属性セレクタの属性値検索文字列は、二重引用符を使用する。
## 非推奨例
```
a[href*='//www.example.com'] {
  font-family: "open sans", arial, sans-serif;
  background-image: url('//www.example.com/link_bg.png');
}
```
## 推奨例
```
a[href*="//www.example.com"] {
  font-family: 'open sans', arial, sans-serif;
  background-image: url(//www.example.com/link_bg.png);
}
```
<br>
<br>

# CSSの少数は省略しない
CSSのプロパティ値に1未満の数値を設定する場合には、CSS仕様上では小数点前の0を省略できるが、明示化のため省略しない。
<br>
<br>
<br>

# CSSの色指定の16進数は6桁で記述する
## 非推奨例
```
color: #ebc;
```
## 推奨例
```
color: #eebbcc;
```
<br>
<br>

# HTMLの書式
* HTMLのドキュメントタイプはHTML5を使用する
* 目的や機能に応じたHTML要素を使用する
* マルチメディアには代替コンテンツを指定する  
  ※ img要素であれば、alt属性を指定する。  
  ※ 目的が単に装飾的な画像の場合はalt=""のような代替テキストの指定は行わない。
* 文書・装飾・動作を分離する  
  ※ HTMLのインラインでの装飾は行わず、CSSにて装飾を行うようにする。  
  ※ javascriptなども同様にインラインで書かない。
* 実態参照文字を乱用しない  
  ※ ファイル・エディタ、及び、製作者は、同じエンコーディング（UTF-8 BOMなし）を使用し、HTMLの中で特別な意味を持つ文字（<や&など）・コントロール（textarea要素への値の入力など）・見えない文字（`&nbsp;`など）を除き、実態参照文字を使用しない。
* 任意タグ・閉じタグは省略しない
* IDとclass名と独自データ属性名の命名規則はそれぞれ統一する。
<br>
<br>
<br>

# CSSの書式
* 最後のセレクタと宣言ブロックの間に、半角スペースを挿入する。
* 複数セレクタを書く際はカンマの後に改行する。
* 宣言ブロックを開始する中括弧は、セレクタと同じ行に記述する。
* セレクタの記述された行とプロパティ宣言の行は改行し、同じ行に記述しない。
* プロパティ宣言冒頭には、セレクタの記述より一つインデントする。
* プロパティ名とコロンの間には、半角スペースを挿入しない。
* プロパティ名後ろのコロンの後に、半角スペースを挿入する。
* プロパティ宣言の行末に、セミコロンを付ける。
* 宣言ブロックを閉じる中括弧は改行し、プロパティ宣言と同じ行に記述しない。
* 宣言ブロックを閉じる中括弧は、セレクタの記述と同一のインデントを付与する。
* 擬似クラス・擬似要素には、シングルコロンを使用する。
* 改行・インデントの書式は、コードの可読性を向上させる場合、無視できるものとする。

## 非推奨例
```
.example ul .one   { width: 100px; }

.example ul .two   {
  width: 80px;
  height: 2em; }

.example ul .three { width: 120px; }

.example ul .four  { width: 160px; }

.example ul .one, .example ul .two {
  display: inline-block;
}
```

## 推奨例
```
.example ul {
  text-align: center;
}
.example ul li {
  display: inline-block;
}
.example ul .one,
.example ul .two {
  display: inline-block;
}
```
<br>
<br>

# 例外
* CMSやスクリプトなどによる自動出力は、例外とする。
* 他のプログラムとの互換性・コード品質・コーダーの互換性向上を目的とした例外を、製作者側製作責任者の了承を得ることで、追加できるものとする。
* 追加された例外は文書とし、当文書とともに保管する。