$(function() {
  //ページトップ処理
  $('.pageTop').click(function(){
    $('html,body').animate({scrollTop:0},800);
  });

  //画像切り替え(switch,sp_half)
  var pcName = '_pc',
    spName = '_sp',
    replaceWidth = 900;
  $('.switch').each(function() {
    var $this = $(this);
    function imgSize() {
      var windowWidth = parseInt($(window).innerWidth());
      if (windowWidth > replaceWidth) {
        $this.attr('src', $this.attr('src').replace(spName, pcName));
      } else if (windowWidth <= replaceWidth) {
        $this.attr('src', $this.attr('src').replace(pcName, spName));
      }
    }
    imgSize();
    $(window).resize(function() {
      imgSize();
    });
  });
});
